import React from 'react';
import {View, Text} from 'react-native';
import SuccessScreen from './SuccessScreen';
import FailureScreen from './FailureScreen';
import PendingScreen from './PendingScreen';

const status = 'failedg';
export default class App extends React.Component {
  render() {
    return (
      <View style={{flex: 1}}>
        {status === 'success' ? (
          <SuccessScreen />
        ) : status == 'failed' ? (
          <FailureScreen />
        ) : (
          <PendingScreen />
        )}
      </View>
    );
  }
}
