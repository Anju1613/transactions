import React from 'react';
import {View, Image, StyleSheet, Text, SafeAreaView} from 'react-native';
import RoundedButton from './RoundedButton';
import PrimaryActionButton from './PrimaryActionButton';
import SecondaryActionButton from './SecondActionButton';

const onViewDetailsClicked = () => {};
const onGoToHomeClicked = () => {};
const onRaiseConcernClicked = () => {};
const onCallBankClicked = () => {};

const BaseTransactionStatusScreen = ({statusImage, statusMessage, details}) => {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.statusImageContainer}>
        <Image style={styles.statusImage} source={statusImage} />
      </View>
      <View style={styles.statusMessageContainer}>
        <Text style={styles.statusMessage}>Payment {statusMessage}</Text>
      </View>

      <View style={styles.detailsMessageContainer}>
        <Text style={styles.detailsMessage}>{details}</Text>
      </View>

      <View style={styles.buttonsContainer}>
        {statusMessage == 'Successful' && (
          <SecondaryActionButton
            action="View Details"
            onPressHandler={() => onViewDetailsClicked}
          />
        )}
        {statusMessage == 'Successful' ? (
          <PrimaryActionButton
            action="Go To Home"
            onPressHandler={() => onGoToHomeClicked}
          />
        ) : (
          <SecondaryActionButton
            action="Go To Home"
            onPressHandler={() => onGoToHomeClicked}
          />
        )}
        {statusMessage == 'Failed' && (
          <PrimaryActionButton
            action="Raise a Concern"
            onPressHandler={() => onRaiseConcernClicked}
          />
        )}
        {statusMessage == 'Pending' && (
          <PrimaryActionButton
            action="Call Bank"
            onPressHandler={() => onCallBankClicked}
          />
        )}
      </View>
    </SafeAreaView>
  );
};

export default BaseTransactionStatusScreen;

const styles = StyleSheet.create({
  container: {
    marginStart: 18,
    marginEnd: 16,
    flex: 1,
  },
  statusImageContainer: {
    flex: 10,
    justifyContent: 'flex-end',
    marginBottom: 17,
  },
  statusImage: {
    height: 95,
    width: 112,
    resizeMode: 'contain',
  },
  statusMessageContainer: {
    flex: 1, 
    marginBottom: 16
  },
  statusMessage: {
    color: '#484848',
    fontFamily: 'HelveticaNeue',
    fontWeight: 'bold',
    fontSize: 32,
    letterSpacing: 0,
  },
  detailsMessageContainer: {
    flex: 2,
    marginBottom: 39,
  },
  detailsMessage: {
    fontFamily: 'HelveticaNeue',
    color: '#484848',
    fontSize: 20,
    lineHeight: 27,
    letterSpacing: 0,
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 16,
    flex: 1,
  },
});
