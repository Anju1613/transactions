import React from 'react';
import BaseTransactionStatusScreen from './BaseTransactionStatusScreen';

const FailureScreen = () => {
  const billerName = "ABCD"
  return (
    <BaseTransactionStatusScreen
      statusImage={require('./assets/failure.png')}
      statusMessage="Failed"
      details={`There was a problem sending money to ${billerName}.Please try again.`}
    />
  );
};
export default FailureScreen;
