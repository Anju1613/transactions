import React from 'react';
import {Text, TouchableOpacity, StyleSheet} from 'react-native';

const RoundedButton = ({
  text,
  onPressHandler,
  buttonColor = [],
  textColor = [],
}) => (
  <TouchableOpacity
    onPress={onPressHandler}
    style={[styles.button, ...buttonColor]}>
    <Text style={[styles.buttonText, ...textColor]}>{text}</Text>
  </TouchableOpacity>
);

export default RoundedButton;

const styles = StyleSheet.create({
  button: {
    height: 40,
    width: 152,
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontFamily: 'HelveticaNeue-Bold',
    fontSize: 16,
    lineHeight: 16,
    letterSpacing: 0,
  },
  
});
