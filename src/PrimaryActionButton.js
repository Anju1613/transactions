import React from 'react';
import {StyleSheet} from 'react-native';
import RoundedButton from './RoundedButton';

const PrimaryActionButton = ({action, onPressHandler}) => (
  <RoundedButton
    text={action}
    onPressHandler={onPressHandler}
    buttonColor={[styles.buttonColor]}
    textColor={[styles.textColor]}
  />
);

export default PrimaryActionButton;

const styles = StyleSheet.create({
  textColor: {
    color: '#ffffff',
  },
  buttonColor: {
    backgroundColor: '#2b4089',
  },
});
