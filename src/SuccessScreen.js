import React from 'react';
import BaseTransactionStatusScreen from './BaseTransactionStatusScreen';

const SuccessScreen = () => {
  const billAmount = 1000.0,
    billerName = 'Pooja Store';
  return (
    <BaseTransactionStatusScreen
      statusImage={require('./assets/success.png')}
      statusMessage="Successful"
      details={`You have successfully paid ₹${parseFloat(
        billAmount,
      ).toFixed(2)} to ${billerName}.`}
    />
  );
};
export default SuccessScreen;
