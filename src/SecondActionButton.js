import React from 'react';
import {StyleSheet} from 'react-native';
import RoundedButton from './RoundedButton';

const SecondaryActionButton = ({action, onPressHandler}) => (
  <RoundedButton
    text={action}
    onPressHandler={onPressHandler}
    buttonColor={[styles.buttonColor]}
    textColor={[styles.textColor]}
  />
);

export default SecondaryActionButton;

const styles = StyleSheet.create({
  textColor: {
    color: '#404040',
  },
  buttonColor: {
    backgroundColor: '#e8e8e8',
  },
});
