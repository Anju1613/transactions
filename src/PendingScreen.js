import React from 'react';
import BaseTransactionStatusScreen from './BaseTransactionStatusScreen';

const PendingScreen = () => {
  return (
    <BaseTransactionStatusScreen
      statusImage={require('./assets/pending.png')}
      statusMessage="Pending"
      details="There was a problem with completing the payment. Please check after a while."
    />
  );
};
export default PendingScreen;
