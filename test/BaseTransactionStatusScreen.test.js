import React from 'react';
import {shallow} from 'enzyme';
import {Image, TouchableOpacity,Text} from 'react-native';
import BaseTransactionStatusScreen from '../src/BaseTransactionStatusScreen';

let wrapper;
beforeAll(() => wrapper = shallow(<BaseTransactionStatusScreen />));

describe('transaction status', () => {
  describe('render', () => {

    it('should render transaction status screen', () => {
      expect(wrapper).not.toBe(null);
    });

    it('should render an Image component', () => {
      expect(wrapper.find(Image)).toBeTruthy();
    });

    it('should render success image on successful transaction', () => {
      expect(
        wrapper
          .find(Image)
          .props().source,
      ).toEqual({testUri: '../../../src/assets/success.png'});
    });

    it('should render status message', () => {
      expect(wrapper.contains('Payment Successful')).toBeTruthy();
    });

    it('should render details', () => {
      const billAmount = 1000.0,
        billerName = 'Pooja Store';
        console.log(wrapper.find(Text).at(1).debug())
      expect(
        wrapper.contains([
          'You have successfully paid ',
          `₹${parseFloat(billAmount).toFixed(2)}`,
          ' to ',
          `${billerName}`,
          '.',
        ]),
      ).toBeTruthy();
    });

  });
});
