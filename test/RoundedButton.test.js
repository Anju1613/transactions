import React from 'react';
import {shallow} from 'enzyme';
import { TouchableOpacity, Text} from 'react-native';
import RoundedButton from '../src/RoundedButton';

beforeAll(() => (wrapper = shallow(<RoundedButton />)));

describe('transaction status', () => {
    describe('render', () => {
        it('should render text', () => {
            const wrapper = shallow(<RoundedButton text="click" />);
            expect(
                wrapper
                .find(TouchableOpacity)
                .children()
                .find(Text)
                .contains('click'),
                ).toBeTruthy();
            }); 
        });
        
        describe('interaction', () => {
          it('should call onPressHandler', () => {
            const handler = jest.fn();
            const wrapper = shallow(<RoundedButton onPressHandler={handler} />);
      
            wrapper
              .find(TouchableOpacity)
              .simulate('press');
            expect(handler).toHaveBeenCalled();
          });
        });
    })