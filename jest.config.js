module.exports={
    preset:'react-native',
    collectCoverage: true,
    setupFiles: ['./test/setup.js'],
    testEnvironment: 'jsdom'
}